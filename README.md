# AC 4 - Dockerfile

> Com base no [tutorial do Nodejs](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)

### Integrantes
- Pedro Barcellos dos Reis
- Rafael Seragioli
- Ronaldo dos Santos Silva

## Como executar o projeto
- Crie um projeto Node simples com o nome __app.js__
- Não esqueça de incluir os arquivos do npm com o comando
``npm init -y``

- Faça o build
```bash
docker build . -t <your username>/node-web-app
```

- Rode a imagem
```bash
docker run -p 49160:8080 -d <your username>/node-web-app
```

## Aplicação exemplo
```js
const express = require('express');
const PORT = 8080;
const HOST = '0.0.0.0';
const app = express();
app.get('/', (req, res) => {
	res.send('Hello World');
});
app.listen(PORT, HOST, () => {
	console.log(`Running on http://${HOST}:${PORT}`);
});
```
